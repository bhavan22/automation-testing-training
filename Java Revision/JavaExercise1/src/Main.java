public class Main {

	public static void main(String[] args) {
		// concatenation of two strings
		String str1 = "hot";
		String str2 = "dog";
		System.out.println("1. Concatenation of two Strings hot and dog: " + str1 + str2 + "\n");
		// find the greatest number from three numbers
		int greatestNum = getGreatestNum(23, 12, 19);
		System.out.println("2. Gretest number from three number(23,12,19) is: " + greatestNum);
		greatestNum = getGreatestNum(122, 98, 377);
		System.out.println("2. Gretest number from three number(122,98,377) is: " + greatestNum + "\n");
		// print the fibonacci series
		printFibonacciNum(8);
		printFibonacciNum(25);
		System.out.println("\n");

		// find the greatest number of three and four by overloading functions
		System.out.println(
				"4. Gretest number from three number by Overloding method is: " + findGreatestNumber(1, 10, 3));
		System.out.println(
				"4. Gretest number from four number by Overloding method is: " + findGreatestNumber(52, 5, 3, 4));
	}

	public static int getGreatestNum(int num1, int num2, int num3) {
		if (num1 > num2 && num1 > num3) {
			return num1;
		} else if (num2 > num1 && num2 > num3) {
			return num2;
		} else if (num3 > num1 && num3 > num2) {
			return num3;
		}
		return num1;
	}

	public static void printFibonacciNum(int count) {
		String printString = "";
		int num1 = 0;
		int num2 = 1;
		int sum = 0;
		for (int i = 0; i < count; i++) {
			sum = num1 + num2;
			num1 = num2;
			num2 = sum;
			if (i != 0)
				printString = printString + ", " + String.valueOf(num2);
			else
				printString = printString + String.valueOf(num2);

		}
		System.out.println("3. Fibonacci Series till number " + count + " is: ");
		System.out.println(printString);
	}

	public static int findGreatestNumber(int num1, int num2, int num3) {
		if (num1 > num2 && num1 > num3) {
			return num1;
		} else if (num2 > num1 && num2 > num3) {
			return num2;
		} else if (num3 > num1 && num3 > num2) {
			return num3;
		}
		return num1;

	}

	public static int findGreatestNumber(int num1, int num2, int num3, int num4) {
		if (num1 > num2 && num1 > num3 && num1 > num4) {
			return num1;
		} else if (num2 > num1 && num2 > num3 && num2 > num4) {
			return num2;
		} else if (num3 > num1 && num3 > num2 && num3 > num4) {
			return num3;
		} else if (num4 > num1 && num4 > num2 && num4 > num3) {
			return num4;
		}
		return num1;
	}

}