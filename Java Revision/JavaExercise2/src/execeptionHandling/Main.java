package execeptionHandling;

public class Main {

	public static void main(String[] args) {
		// passing the denominator as 0
		Devide(1, 0);
		// passing the one string as null
		Concate(null, "try");

	}

	public static void Devide(int x, int y) {
		int d = 0;
		try {
			d = x / y;
			System.out.println("Devision of " + x + "/" + y + "is " + d);
		} catch (ArithmeticException e) {
			e.printStackTrace();
			System.err.println("Printing Arithematic execption when denominator is 0 here pass value are " + x + ", "
					+ y + " " + e);
		}

	}

	public static void Concate(String str1, String str2) {
		try {

			System.out.println(str1.concat(str2));
		} catch (NullPointerException e) {
			System.err.println("Printing nullpointer execption error if passed string is null" + e);
		}
	}

}
