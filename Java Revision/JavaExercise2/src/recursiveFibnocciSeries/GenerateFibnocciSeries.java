package recursiveFibnocciSeries;

public class GenerateFibnocciSeries {
	public static void main(String[] args) {
		System.out.println("Fibnocci Series till number 5 is: " + getSeries(5));
		System.out.println("Fibnocci Series till number 10 is: " + getSeries(10));

	}

	public static String getSeries(int n) {
		String series = "";
		for (int i = 1; i <= n; i++) {
			if (i != 1)
				series = series + ", " + getFibonaccisum(i);
			else {
				series = series + getFibonaccisum(i);
			}
		}
		return series;
	}

	public static int getFibonaccisum(int n) {

		if (n == 1) {
			return 1;
		} else if (n == 2) {
			return 2;
		} else {
			return getFibonaccisum(n - 2) + getFibonaccisum(n - 1);
		}
	}
}
