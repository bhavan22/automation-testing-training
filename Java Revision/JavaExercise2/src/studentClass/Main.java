package studentClass;

public class Main {

	public static void main(String[] args) {
		// 1st object creation
		int randomsID = (int) Math.floor(Math.random() * (50 - 10 + 1) + 10);
		Student s1 = new Student(randomsID, "Bhavan", "C", "Patel", "12/03/1999");
		s1.displayFullname();
		s1.displayDateofBirth();
		System.out.println("\n");
		// 2st object creation
		randomsID = (int) Math.floor(Math.random() * (50 - 10 + 1) + 10);
		Student s2 = new Student(randomsID, "Mira", "K", "Bell", "11/01/1994");
		s2.displayFullname();
		s2.displayDateofBirth();
		System.out.println("\n");
		// 3st object creation
		randomsID = (int) Math.floor(Math.random() * (50 - 10 + 1) + 10);
		Student s3 = new Student(randomsID, "Nick", "J", "John", "01/04/2000");
		s3.displayFullname();
		s3.displayDateofBirth();
		System.out.println("\n");

	}

}
