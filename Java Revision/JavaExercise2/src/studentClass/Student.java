package studentClass;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Student {

	private int studentID;
	private String first_name;
	private String midlle_name;
	private String last_name;
	private String dobDate;

	public Student(int studentID, String first_name, String midlle_name, String last_name, String dobDate) {
		super();
		this.studentID = studentID;
		this.first_name = first_name;
		this.midlle_name = midlle_name;
		this.last_name = last_name;
		this.dobDate = dobDate;
	}

	public int getStudentID() {
		return studentID;
	}

	public void setStudentID(int studentID) {
		this.studentID = studentID;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMidlle_name() {
		return midlle_name;
	}

	public void setMidlle_name(String midlle_name) {
		this.midlle_name = midlle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getdOBDate() {
		return dobDate;
	}

	public void setdOBDate(String dobDate) {
		this.dobDate = dobDate;
	}

	public void displayFullname() {
		System.out.println("Student's full name is: " + first_name + " " + midlle_name + " " + last_name);
	}

	public void displayDateofBirth() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate date = LocalDate.parse(dobDate, formatter);
		String fomratedString = date.format(DateTimeFormatter.ofPattern("YYYY/dd/MM"));
		System.out.println("Student's DOB is: " + fomratedString);

	}
}
