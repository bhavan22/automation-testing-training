package compareingObjects;

// this is the car object 
public class Car implements Comparable<Car> {
	private String carmake;
	private String carName;

	public Car(String carmake, String carName) {
		super();
		this.carmake = carmake;
		this.carName = carName;
	}

	public String getCarmake() {
		return carmake;
	}

	public void setCarmake(String carmake) {
		this.carmake = carmake;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	// Declare the compareTo method to override the interface's compareTo method
	@Override
	public int compareTo(Car objCar) {
		// comparing this object with the passed object
		if (this.carmake.equals(objCar.carmake) && this.carName.equals(objCar.carName)) {
			// return 1 if true
			return 1;
		} else {
			// return 0 if flase
			return 0;
		}

	}

}
