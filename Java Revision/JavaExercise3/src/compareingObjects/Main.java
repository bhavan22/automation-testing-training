package compareingObjects;

public class Main {

	public static void main(String[] args) {
		// initialize first object
		Car obj1 = new Car("Honda", "Civic");
		// initialize second object
		Car obj2 = new Car("Honda", "Civic");
		// initialize third object
		Car obj3 = new Car("Honda", "Accord");
		// compare the objects
		// using compareTo
		if (obj1.compareTo(obj2) == 1) {
			System.out.println("Obj1 is equal to obj2");
		} else {
			System.out.println("Obj1 is not equal to obj2");
		}
		if (obj1.compareTo(obj3) == 1) {
			System.out.println("Obj1 is equal to obj3");
		} else {
			System.out.println("Obj1 is not equal to obj3");
		}

	}

}
