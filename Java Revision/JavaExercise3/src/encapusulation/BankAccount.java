package encapusulation;
//this is the class that represent the wrapping class for encapuslation

public class BankAccount {
	// here is the data variable as private
	private String accountName;
	private double accountBalance;

	// below is the methods to operate on the variable
	public BankAccount(String accountName, double accountBalance) {
		super();
		this.accountName = accountName;
		this.accountBalance = accountBalance;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public boolean depositMoney(double depositAmount) {
		accountBalance = accountBalance + depositAmount;
		return true;
	}

	public boolean withdrawMoney(double wMoney) {
		if (wMoney > accountBalance) {
			return false;
		} else {
			accountBalance = accountBalance - wMoney;
			return true;
		}

	}

	public void getAccountDetails() {
		System.out.println("Account Details is:");
		System.out.println("Account name: " + accountName);
		System.out.println("Account balance: " + accountBalance);

	}
}
