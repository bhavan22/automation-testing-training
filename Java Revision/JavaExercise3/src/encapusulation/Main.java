package encapusulation;

public class Main {

	public static void main(String[] args) {
		// Here user is creating the account
		BankAccount userAccount = new BankAccount("Nick", 1200);
		// here the user is doing some action with the account
		userAccount.depositMoney(100);
		if (userAccount.withdrawMoney(1400)) {
			System.out.println("Withdrwa Sucess");
		} else {
			System.out.println("Not Enough fund present");
		}
		// here user is printing the details of the account
		userAccount.getAccountDetails();
		// as result the user is only doing actions but the implemation is hideen.
	}

}
