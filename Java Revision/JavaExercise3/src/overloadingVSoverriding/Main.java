package overloadingVSoverriding;

public class Main {

	public static void main(String[] args) {
		// create the obj of overloading for example
		Overloding oObjOverloding = new Overloding();
		oObjOverloding.print(2);
		oObjOverloding.print("HI");
		oObjOverloding.print("HI", 2);

		// create the object of overrinding
		ParentClassOverriding obj1 = new ParentClassOverriding();
		obj1.printOverriding();
		ParentClassOverriding obj2 = new SubClassOverriding();
		obj2.printOverriding();

	}

}
