package overloadingVSoverriding;

public class Overloding {
	// here this is overloading method code example
	// here the methods name are same
	// but here the argument is string type
	public void print(String message) {
		System.out.println("Printing message Overloading of String: " + message);
	}

	// here argument is int type
	public void print(int message) {
		System.out.println("Printing message Overloading of int: " + message);
	}

	// here number of arguments is 2
	// as a result you can create many methods using this kind of implemation
	public void print(String message, int msg) {
		System.out.println("Printing message Overloading of String and int: " + message + " " + msg);
	}
}
