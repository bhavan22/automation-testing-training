package overloadingVSoverriding;

// here as you can see the parent class method is also printOverriding 
public class ParentClassOverriding {
	public void printOverriding() {
		System.out.println("Printing from the Parent class");
	}

}
