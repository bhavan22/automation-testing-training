package overloadingVSoverriding;

// here sub class method name is also printOverriding
// if the object is created with this class it will call this method 
// this also extends the parent class
public class SubClassOverriding extends ParentClassOverriding {
	public void printOverriding() {
		System.out.println("Printing from the Sub class");
	}

}
