package polymorphism;

public class Main {

	public static void main(String[] args) {
		// CompileTime Polymorphism
		System.out.println("Example of compile time Polymorphism:");
		CompileTimePolymorphism obj = new CompileTimePolymorphism();
		// here the method name is same however the number of arguments is diffrent
		System.out.println(obj.add(2, 3));
		System.out.println(obj.add(1, 2, 3));
		// here the method name is same however the datatye of arguments is diffrent
		System.out.println(obj.add(2.5, 2.5));

		// Runtime Polymorphism
		// here the print method name is the same
		// however the printObj1 is created using Parent Class at run time.
		// so the method will print from there
		// printObj2 is created using the sub class at run time
		// so the method will be printed from the sub class
		System.out.println("Example of Runtime Polymorpihsm:");
		ParentClassPrint printObj1 = new ParentClassPrint();
		ParentClassPrint printObj2 = new SubClassPrint();
		printObj1.print();
		printObj2.print();

	}

}
