package polymorphism;

// this the parent class object that other class will be inherit from
// here the method name is print 
public class ParentClassPrint {
	public void print() {
		System.out.println("Printing from the Parent class");
	}

}
