package polymorphism;

// this is the child class which is inherit from the parent class 
//which is why the extrends keyword is there
//here the method name is print 
public class SubClassPrint extends ParentClassPrint {
	@Override
	public void print() {
		System.out.println("Printing from the Sub class");
	}

}
