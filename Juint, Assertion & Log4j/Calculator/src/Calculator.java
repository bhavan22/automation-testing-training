public class Calculator {
	public int sum(int n1, int n2) {
		int sum = n1 + n2;
		return sum;
	}

	public int diff(int n1, int n2) {
		int dif = n1 - n2;
		return dif;
	}

	public int divide(int n1, int n2) {

		int div = n1 / n2;
		return div;

	}

	public int multiply(int n1, int n2) {
		int multi = n1 * n2;
		return multi;
	}
}
