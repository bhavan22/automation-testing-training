import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CalculatorTests {

	@Test
	void testSum() {
		Calculator calculator = new Calculator();
		assertEquals(5, calculator.sum(2, 3));
	}

	@Test
	void testDiff() {
		Calculator calculator = new Calculator();
		assertEquals(3, calculator.diff(5, 2));
	}

	@Test
	void testDivide() {
		Calculator calculator = new Calculator();
		assertThrows(ArithmeticException.class, () -> calculator.divide(2, 0));
		assertEquals(5, calculator.divide(10, 2));
	}

	@ParameterizedTest
	@CsvSource({ "1,2", "2,3" })
	void testMulti(int n1, int n2) {
		Calculator calculator = new Calculator();
		int expected = n1 * n2;
		int result = calculator.multiply(n1, n2);
		assertEquals(expected, result);

	}

}
