import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");
		WebElement firstnamElement = driver.findElement(By.id("first-name"));
		firstnamElement.sendKeys("Nick");
		WebElement lastNamElement = driver.findElement(By.id("last-name"));
		lastNamElement.sendKeys("John");
		WebElement jobTitlElement = driver.findElement(By.id("job-title"));
		jobTitlElement.sendKeys("SWE-1");
		WebElement eduRadioButton = driver.findElement(By.id("radio-button-3"));
		eduRadioButton.click();
		WebElement sexCheckBox = driver.findElement(By.id("checkbox-1"));
		sexCheckBox.click();
		Select yearsOfExpSelect = new Select(driver.findElement(By.id("select-menu")));
		yearsOfExpSelect.selectByIndex(2);
		WebElement datePickerElement = driver.findElement(By.id("datepicker"));
		datePickerElement.sendKeys("01/02/98");
		datePickerElement.sendKeys(Keys.RETURN);
		WebElement submitButtonElement = driver.findElement(By.cssSelector("a[class='btn btn-lg btn-primary']"));
		submitButtonElement.click();
		Thread.sleep(2000);
		driver.quit();

	}

}
