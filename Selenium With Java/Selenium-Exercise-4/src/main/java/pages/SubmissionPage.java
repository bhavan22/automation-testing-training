package pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SubmissionPage {
	WebDriver driver;

	public SubmissionPage(WebDriver driver) {
		this.driver = driver;

	}

	public String getSubmisionMessage() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		String messageString = driver.findElement(By.cssSelector("div[role='alert']")).getText();
		return messageString;
	}

}
