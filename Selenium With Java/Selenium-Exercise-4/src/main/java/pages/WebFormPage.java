package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class WebFormPage {
	WebDriver driver;
	private String firstname;
	private String lastname;
	private String jobTitle;
	private String education;
	private String sex;
	private int experienceYears;
	private String dateString;

	// WebElements
	WebElement fnameElement;
	WebElement lastnElement;
	WebElement jobElement;
	WebElement edElement;
	WebElement sexElement;
	WebElement yearsElement;
	WebElement datElement;
	WebElement submitButtonElement;

	public WebFormPage(WebDriver driver) {
		this.driver = driver;
		fnameElement = driver.findElement(By.id("first-name"));
		lastnElement = driver.findElement(By.id("last-name"));
		jobElement = driver.findElement(By.id("job-title"));
		submitButtonElement = driver.findElement(By.cssSelector("a[class='btn btn-lg btn-primary']"));
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public String getEducation() {
		return education;
	}

	public String getSex() {
		return sex;
	}

	public int getExperienceYears() {
		return experienceYears;
	}

	public String getDateString() {
		return dateString;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
		fnameElement.sendKeys(firstname);
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
		lastnElement.sendKeys(lastname);
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
		jobElement.sendKeys(jobTitle);
	}

	public void setEducation(String education) {
		this.education = education;
		if (education.equalsIgnoreCase("h")) {
			driver.findElement(By.id("radio-button-1")).click();

		} else if (education.equalsIgnoreCase("c")) {
			driver.findElement(By.id("radio-button-2")).click();
			;

		} else if (education.equalsIgnoreCase("g")) {
			driver.findElement(By.id("radio-button-3")).click();

		}
	}

	public void setSex(String sex) {
		this.sex = sex;
		if (sex.equalsIgnoreCase("m")) {
			driver.findElement(By.id("checkbox-1")).click();

		} else if (sex.equalsIgnoreCase("f")) {
			driver.findElement(By.id("checkbox-2")).click();
		} else {
			driver.findElement(By.id("checkbox-3")).click();
		}
	}

	public void setExperienceYears(int experienceYears) {
		this.experienceYears = experienceYears;
		Select yearsOfExpSelect = new Select(driver.findElement(By.id("select-menu")));
		if (experienceYears > 10) {
			yearsOfExpSelect.selectByIndex(4);
		} else if (experienceYears >= 5 && experienceYears <= 9) {
			yearsOfExpSelect.selectByIndex(3);

		} else if (experienceYears >= 2 && experienceYears <= 4) {
			yearsOfExpSelect.selectByIndex(2);

		} else if (experienceYears >= 0 && experienceYears <= 1) {
			yearsOfExpSelect.selectByIndex(1);

		}
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
		WebElement datePickerElement = driver.findElement(By.id("datepicker"));
		datePickerElement.sendKeys(dateString);
		datePickerElement.sendKeys(Keys.RETURN);
	}

	public boolean submit() {
		submitButtonElement.click();
		return true;
	}

}
