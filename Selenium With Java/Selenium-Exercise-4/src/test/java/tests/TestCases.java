package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.SubmissionPage;
import pages.WebFormPage;

@FixMethodOrder(MethodSorters.DEFAULT)
public class TestCases {

	WebDriver driver;
	SubmissionPage submissionPage;
	WebFormPage formPage;

	@Before
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");
	}

	@Test
	public void pagetitleTest() {
		// System.out.println(driver.getTitle());
		assertEquals("Formy", driver.getTitle());
	}

	@Test
	public void fillFormTestAndConformationTest() {
		formPage = new WebFormPage(driver);
		formPage.setFirstname("Nick");
		formPage.setDateString("01/02/2001");
		// g for grade
		formPage.setEducation("g");
		formPage.setLastname("John");
		formPage.setJobTitle("SWE-1");
		// m is for male
		formPage.setSex("m");
		formPage.setExperienceYears(2);
		assertTrue(formPage.submit());
		submissionPage = new SubmissionPage(driver);
		assertEquals("The form was successfully submitted!", submissionPage.getSubmisionMessage());

	}

	@After
	public void quit() throws InterruptedException {
		driver.close();
	}
}
