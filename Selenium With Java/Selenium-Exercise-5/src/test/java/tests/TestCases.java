package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.SubmissionPage;
import pages.WebFormPage;

@FixMethodOrder(MethodSorters.DEFAULT)

public class TestCases {

	WebDriver driver;
	SubmissionPage submissionPage;
	WebFormPage formPage;
	DataFormatter formatter = new DataFormatter();

	@Before
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
		driver = new ChromeDriver();

	}

	@Test
	public void fillFormTestAndConformationTest() throws IOException, InterruptedException {

		FileInputStream fileInputStream = new FileInputStream(
				"/Users/bhavan/Desktop/Eclipse Projetcs/Workspace 1/Selenium-Exercise-4/src/test/resources/Book1.xlsx");
		@SuppressWarnings("resource")
		XSSFWorkbook wb1 = new XSSFWorkbook(fileInputStream);
		XSSFSheet sheet = wb1.getSheet("Sheet1");
		int rowCount = sheet.getPhysicalNumberOfRows();
		XSSFRow row = sheet.getRow(0);
		int colCount = row.getLastCellNum();
		System.out.println(colCount);

		System.out.println(rowCount);
		for (int i = 1; i < rowCount; i++) {
			row = sheet.getRow(i);

			String fnString = formatter.formatCellValue(row.getCell(0));
			String lnameString = formatter.formatCellValue(row.getCell(1));
			String jobString = formatter.formatCellValue(row.getCell(2));
			String edString = formatter.formatCellValue(row.getCell(3));
			String sexString = formatter.formatCellValue(row.getCell(4));
			String experinceString = formatter.formatCellValue(row.getCell(5));
			String dateString = formatter.formatCellValue(row.getCell(6));

			driver.get("https://formy-project.herokuapp.com/form");
			formPage = new WebFormPage(driver);
			formPage.setFirstname(fnString);
			formPage.setLastname(lnameString);
			formPage.setDateString(dateString);
			formPage.setEducation(edString);
			formPage.setJobTitle(jobString);
			formPage.setSex(sexString);
			formPage.setExperienceYears(Integer.parseInt(experinceString));
			Thread.sleep(15);
			assertTrue(formPage.submit());
			submissionPage = new SubmissionPage(driver);
			assertEquals("The form was successfully submitted!", submissionPage.getSubmisionMessage());

		}

	}

	@After
	public void quit() throws InterruptedException {
		driver.close();
	}

}
